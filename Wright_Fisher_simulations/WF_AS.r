#loops over each SNP freq (read from file) 
snps=read.csv("SNPex.csv",header=T)
attach(snps)
total_snps=nrow(snps)
df = data.frame()
for (k in 1:total_snps){
N=as.numeric(snps[k,2])
j=as.numeric(snps[k,3])
af <- vector("numeric", 100)  #if changed, must match below in for counter
for (i in 1:100){
  m=round(runif(1, min=2, max=10),0) #adjust m as you see fit
  af[i]=wright_fisher(N,m,j)
}
df=rbind(df,af)
}
####################################################################
#run above loop, to add mean, min, max or 95%CI to file and whether SNP is due to drif or is an outlier, run below loops
#std <- function(x) sd(x)/sqrt(length(x)) #needed if want to calculate 95%CI
total_snps=nrow(df)
sim_final<- data.frame()
for (l in 1:total_snps){
sim_AF=mean(as.numeric(df[l,]))
simSE=std(df[l,])
#sim_lower=sim_AF-qnorm(0.975)*(simSE/sqrt(100)) #change if not 100 as per above
#sim_upper=sim_AF+qnorm(0.975)*(simSE/sqrt(100))
sim_lower=min(as.numeric(df[l,]))
sim_upper=max(as.numeric(df[l,]))
temp=cbind(sim_AF,sim_lower,sim_upper)
sim_final=rbind(sim_final,temp)
}
#######################################
temp=cbind(snps,sim_final)
status<-NULL
df_FINAL<- data.frame()
for (i in 1:nrow(temp)){
  if (temp[i,5] < temp[i,7] | temp[i,5] >temp[i,8]){
    #print("outlier")
    status[i]<-"outlier"
    } else {
    #print("drift")
    status[i]<-"drift"
    }
}
df_FINAL=cbind(temp,status)
df_FINAL    

