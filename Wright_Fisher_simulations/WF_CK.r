snpex <- read.table("SNPex.csv", stringsAsFactors = FALSE, 
                    header = TRUE, sep = ",")

library(stats)
for (locus in 1:nrow(snpex)) {
  sim <- c()
  N <- snpex[locus, "N"]
  j <- snpex[locus, "j"]
  for (i in 1:100){
    m = round(runif(1, min = 2, max = 10), 0)
    sim[i] <- wright_fisher(N, m, j)
  }
  
  snpex[locus, "sim_mean"] <- mean(sim)
  snpex[locus, "sim_95CI"] <- qt(0.975, df = N)*sd(sim)/sqrt(N)
  snpex[locus, "sim_lower"] <- mean(sim) - snpex[locus, "sim_95CI"]
  snpex[locus, "sim_upper"] <- mean(sim) + snpex[locus, "sim_95CI"]
  snpex[locus, "sim_max"] <- max(sim)
  snpex[locus, "sim_min"] <- min(sim)
  
  if (snpex[locus, "sim_mean"] < snpex[locus, "sim_lower"] | 
      snpex[locus, "sim_mean"] > snpex[locus, "sim_lower"]){
    snpex[locus, "status"] <- "outlier"
  } else {
    snpex[locus, "status"] <- "drift"
  }
}
